<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400" rel="stylesheet">
<title>Stats - WELTEINZ</title>
<style>
table {
border-collapse: collapse;
width: 100%;
color: #5e9696;
font-family: 'Montserrat';
font-weight: 400;
font-size: 25px;
text-align: left;
}
th {
background-color: #bf5a6b;
color: white;
}
tr:nth-child(even) {background-color: #1b1b1b}
tr:nth-child(odd) {background-color: #3f3f3f}
</style>
</head>
<body bgcolor="1b1b1b">
<?php
include 'variables.php';
$conn = mysqli_connect($mysqlhost, $mysqluser, $mysqlpw, $mysqldb);
// Check connection
if ($conn->connect_error) {
echo "Connection failed: " . $conn->connect_error;
exit();
}
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_playtime
WHERE uuid = player
ORDER BY amount DESC
LIMIT 3";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
$i = 1;
$username = array();
$amount = array();
while($row = $result->fetch_assoc()) {
$username[$i] = $row["username"];
$amount[$i] = $row["amount"];
$i++;
}
// echo $amount[2];
echo "</table>";
} else { echo "0 results"; }
?>
	
<div id="piechart", align="center"></div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

var username1 = "<?php echo"$username[1]"?>";
var username2 = "<?php echo"$username[2]"?>";
var username3 = "<?php echo"$username[3]"?>";
var amount1 = "<?php echo"$amount[1]"?>";
var amount2 = "<?php echo"$amount[2]"?>";
var amount3 = "<?php echo"$amount[3]"?>";

function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Player', 'Playtime in Seconds'],
  [username1, parseInt(amount1)],
  [username2, parseInt(amount2)],
  [username3, parseInt(amount3)]
]);
  var options = {'width':550, 'height':400,
  colors: ['#be5969', '#5e9696', '#3f3f3f'],
  backgroundColor: '#1b1b1b',
  is3D: true,
  legend: 'none',
  pieSliceText: 'label',
  pieSliceTextStyle: {
	  fontName: 'Montserrat',
	  fontSize: 15,
	  bold: true
  }
};
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>
</body>
<table>
<tr>
<th>Player</th>
<th>Died</th>
<th>Killed by</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, cause
FROM stats_players, stats_death
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["cause"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Mined</th>
<th>Material</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_break
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["material"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Mined Diamond Ore</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_break
WHERE uuid = player AND material = 'minecraft:diamond_ore'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Placed Wither Skeleton Skulls</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_place
WHERE uuid = player AND material = 'minecraft:wither_skeleton_skull'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Placed Redstone Dust</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_place
WHERE uuid = player AND material = 'minecraft:redstone_wire'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Placed beacon</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_place
WHERE uuid = player AND material = 'minecraft:beacon'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Arrows shot</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_arrows_shot
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Killed victim</th>
<th>Amount</th>
<th>By</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, victimName, weapon
FROM stats_players, stats_kill
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["victimName"] . "</td><td>"
. $row["amount"] . "</td><td>". $row["weapon"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Buckets emptied</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_buckets_emptied
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Eggs thrown</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_eggs_thrown
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Items crafted</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_crafted
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Items dropped</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_dropped
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Items picked up</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Diamonds picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:diamond'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Wither Skeleton Skulls picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:wither_skeleton_skull'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Shulker Shells picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:shulker_shell'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Totems of Undying picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:totem_of_undying'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Elytras picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:elytra'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Dragon Heads picked up</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_items_picked_up
WHERE uuid = player AND type = 'minecraft:dragon_head'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Moved in meters</th>
<th>By</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_move
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Flown in meters</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_move
WHERE uuid = player AND type = 'Gliding'
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Teleports</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_teleports
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Times kicked</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_times_kicked
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Playtime in seconds</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_playtime	
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Shortest Playtime in seconds</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_playtime
WHERE uuid = player
ORDER BY amount ASC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Times sheared</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_times_sheared
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Tools broken</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_tools_broken
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Words said</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_words_said
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Placed</th>
<th>Material</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, material
FROM stats_players, stats_block_place
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["material"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Beds entered</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_beds_entered
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Damage taken</th>
<th>By</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_damage_taken
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Fish caught</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_fish_caught
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Food consumed</th>
<th>Type</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount, type
FROM stats_players, stats_food_consumed
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td><td>"
. $row["type"]. "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>Last join</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, timestamp
FROM stats_players, stats_last_join
WHERE uuid = player
ORDER BY timestamp DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["timestamp"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
?>
</body>
<body>
<table>
<tr>
<th>Player</th>
<th>XP gained</th>
</tr>
<?php
$sql = "SELECT username, uuid, player, amount
FROM stats_players, stats_xp_gained
WHERE uuid = player
ORDER BY amount DESC
LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["username"]. "</td><td>" . $row["amount"] . "</td></tr>";
}
echo "</table>";
} else { echo "0 results"; }
$conn->close();
?>
</body>
</html>
